package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.example.model.WikiMediaData;
import com.example.repository.WikiMediaDataRepo;

@Service
public class KafkaDatabaseConsumer {

	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaDatabaseConsumer.class);
	
	
	private WikiMediaDataRepo wikiMediaDataRepo;
	
	public KafkaDatabaseConsumer(WikiMediaDataRepo wikiMediaDataRepo) {
		super();
		this.wikiMediaDataRepo = wikiMediaDataRepo;
	}


	@KafkaListener(topics="wikimedia_kafka" , groupId="myGroup")
	public void consume(String eventMessage) {
		
		LOGGER.info(String.format("Event message recieved -> %s", eventMessage));
		WikiMediaData wikiMediaData = new WikiMediaData();
		wikiMediaData.setWikiEventData(eventMessage);
		
	wikiMediaDataRepo.save(wikiMediaData);
	
	}
	
	
}
