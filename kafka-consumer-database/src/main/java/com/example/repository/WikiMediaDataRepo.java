package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.WikiMediaData;

public interface WikiMediaDataRepo extends JpaRepository<WikiMediaData, Long> {

	
}
